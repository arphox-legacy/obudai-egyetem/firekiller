﻿using System;
using System.Collections.Generic;   //List<T>
using System.Windows;               //Window
using System.Windows.Controls;      //Grid
using System.Windows.Input;         //Key, KeyEventArgs
using System.Windows.Media;         //MediaPlayer
using System.Windows.Media.Imaging; //BitmapImage

namespace FireKiller_GLBXQU
{
    public partial class MainWindow : Window
    {
        /// <summary>
        /// A grid előző állapotának mentésére szolgál.
        /// </summary>
        Grid gridsave;
        /// <summary>
        /// Ide tárolódnak el a textúracsomagok nevei.
        /// </summary>
        List<string> textúracsomagok;

        public MainWindow()
        {
            InitializeComponent();
            Settings.Beállítás_Betöltés();                          //Beállítások betöltése
            Settings.Hibaellenőrzés();                              //Hibás fájlok ellenőrzése
            textúracsomagok = Settings.TextúraCsomagokBetöltése();  //Textúra csomagok lista feltöltése
        }

        #region Háttérzene

        /// <summary>
        /// MediaPlayer a háttérzene lejátszásához
        /// </summary>
        MediaPlayer mediaPlayer = new MediaPlayer();
        /// <summary>
        /// Megnyitja az intro.mp3 fájlt, feliratkoztatja a HáttérZeneEnded metódust a zene végét jelző
        /// eseményre, és ha be van állítva, akkor elindítja a háttérzenét.
        /// </summary>
        private void HáttérZene()
        {
            mediaPlayer.Open(new Uri(@"music/intro.mp3", UriKind.Relative)); //megnyitja az intro.mp3 fájlt
            mediaPlayer.MediaEnded += HáttérZeneEnded; //a fájl végére érve az elejéről kell kezdeni a lejátszást, ehhez feliratkozik a megfelelő eseményre
            if (Settings.Háttérzene) //ha a háttérzene be van állítva igazra, akkor
                HáttérZeneIndít(); //elindítja a háttérzenét
        }
        /// <summary>
        /// Ha a zene végére érünk, ez a metódus indítja újra az elejétől a lejátszást.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HáttérZeneEnded(object sender, EventArgs e)
        {
            mediaPlayer.Position = TimeSpan.Zero; //ehhez elegendő csak a pozíciót nullára állítani
        }
        /// <summary>
        /// Igazra állítja a háttérzene beállítást, és elindítja a zenét.
        /// </summary>
        private void HáttérZeneIndít()
        {
            Settings.Háttérzene = true;
            mediaPlayer.Play();
        }
        /// <summary>
        /// Pause-olja a lejátszót, és hamisra állítja a háttérzene beállítást.
        /// </summary>
        private void HáttérZeneSzünetel()
        {
            mediaPlayer.Pause();
            Settings.Háttérzene = false;
        }

        #endregion Háttérzene

        #region Egyéb metódusok

        /// <summary>
        /// Készít egy Vissza gombot (ezzel tér vissza), amelyet megformáz és feliratkoztatja a
        /// ButtonVissza_Click metódusát a ButtonVissza.Click eseményre.
        /// </summary>
        /// <returns></returns>
        private Button VisszaButton_Készít()
        {
            Button ButtonVissza = new Button();
            ButtonVissza.Content = "Vissza";
            ButtonVissza.HorizontalAlignment = HorizontalAlignment.Center;
            ButtonVissza.Margin = new Thickness(0, 437, 0, 0);
            ButtonVissza.Width = 250;
            ButtonVissza.Height = 40;
            StílustFormázButton(ButtonVissza);
            ButtonVissza.Click += ButtonVissza_Click;
            return ButtonVissza;
        }
        /// <summary>
        /// Készít egy Image-t (ezzel tér vissza). Ennek forrása a /Images/logo.png fájl.
        /// </summary>
        /// <returns></returns>
        private Image Image_Készít()
        {
            Image img = new Image();
            img.Source = new BitmapImage(new Uri(@"/Images/logo.png", UriKind.Relative));
            img.Margin = new Thickness(0, 0, 0, 439);
            return img;
        }

        #endregion

        #region Button Click események

        /// <summary>
        /// Az új játék gomb lenyomásakor lefutó metódus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonUjJatek_Click(object sender, RoutedEventArgs e)
        {
            Palya p = new Palya();                          //csinál egy új pályát
            JatekWindow játékablak = new JatekWindow(p);    //és egy új JatekWindow-t, aminek átadja a pályát.
            mediaPlayer.Stop();                             //megállítja a főablakban történő zenelejátszást
            foablak.Hide();                                 //elrejti a főablakot
            játékablak.ShowDialog();                        //modálisan megjeleníti a játékablakot
            if (játékablak.Játékos.ÉletbenVan)              //ha a játékos életben van az ablak bezárása után
                ButtonUjJatek.Content = String.Format("folytatás ({0}.pálya)", Settings.HanyadikPálya); //az új játék gomb felirata folytatás lesz
            else
                ButtonUjJatek.Content = "Új játék";         //különben pedig "Új játék"
            foablak.Show();                                 //ezután megjeleníti a főablakot
            if (Settings.Háttérzene)                        //és szükség esetén elindítja a háttérzenét.
            {
                mediaPlayer.Position = TimeSpan.Zero;       //elölről.
                HáttérZeneIndít();
            }
        }
        /// <summary>
        /// A beállítások gomb lenyomásakor lefutó metódus.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBeallitasok_Click(object sender, RoutedEventArgs e)
        {
            gridsave = foablak.gridFoablak;                 //elmenti a grid aktuális állapotát a változóba
            Grid ujGrid = new Grid();                       //csinál egy új gridet, amit majd fel fog tölteni
            ujGrid.Background = Brushes.Black;              //az új grid hátterét beállítja feketére
            ujGrid.Children.Add(Image_Készít());            //az új gridhez hozzáadja a logot az Image_Készít() metódussal
            ujGrid.Children.Add(VisszaButton_Készít());     //és a Vissza gombot is.

            #region Fő StackPanel

            StackPanel sp = new StackPanel();               //Létrehozza a StackPanelt, ahova majd kerülnek az elemek
            sp.Margin = new Thickness(10, 0, 0, 0);         //beállít egy alap margót a bal oldalra

            #endregion
            #region Háttérzene beállító CheckBox

            CheckBox CheckBoxHatterzene = new CheckBox();   //Létrehozza a háttérzene beállításához szükséges checkboxot
            StílustFormázCheckBox(CheckBoxHatterzene, " Háttérzene", 25, Brushes.Red, new Thickness(10, 150, 0, 5)); //alkalmazza rá a programra jellemző stílust
            if (!Settings.Ellenőrző_music())  //Ha a forrásfájlok között nem megfelelőek a hangfájlok, akkor
            {
                Settings.Háttérzene = false;                //kikapcsolja a háttérzenét
                CheckBoxHatterzene.Content = "(hiányzó fájlok!)"; //a CheckBox szövege "(hiányzó fájlok!)" lesz
                CheckBoxHatterzene.Foreground = Brushes.LightGray;  //a feliratot kiszürkíti
                CheckBoxHatterzene.IsEnabled = false;               //és deaktiválja a checkboxot
            }
            CheckBoxHatterzene.IsChecked = Settings.Háttérzene ? true : false;  //lekéri a hátérzene állapotát a beállításokból és ennek megfelelően beállítja
            CheckBoxHatterzene.Checked += hatterzene_Checked;       //a Checked és Unchecked eseményekre
            CheckBoxHatterzene.Unchecked += hatterzene_Unchecked;   //feliratkoztatja a megfelelő metódusokat

            sp.Children.Add(CheckBoxHatterzene);            //hozzáadja a fő stackpanelhoz a checkboxot
            
            #endregion
            #region Textúracsomag-választás

            StackPanel spTextúra = new StackPanel();        //létrehoz egy stackpanelt annak érdekében, hogy a benne lévő két elem egymás mellett lehessen
            spTextúra.Orientation = Orientation.Horizontal; //beállítja az orientációt horizontálisra
            spTextúra.Margin = new Thickness(10, 0, 0, 0);  //beállít egy 10-es bal margót

            Label LabelTextúraCsomag = new Label();         //létrehozza a feliratot kínáló labelt
            StílustFormázLabel(LabelTextúraCsomag, "Textúracsomag: " , 25, 120, new Thickness(0, 5, 0, 5)); //a labelt megformázza a játék stílusának megfelelő módon
            spTextúra.Children.Add(LabelTextúraCsomag);     //a stackpanel gyermekeihez hozzáadja a labelt

            ComboBox ComboBoxTexturacsomagok = new ComboBox();  //elkészíti a textúracsomagok comboboxot
            StílustFormázComboBox(ComboBoxTexturacsomagok, 25, 200, new Thickness(20, 5, 0, 5)); //a comboboxot megformázza a játék stílusának megfelelő módon

            if (Settings.BackupTextúraCsomagHasználata)     //ha tartalék textúracsomagot használunk
            {//akkor a combobox felirata "hiányzó fájlok!" lesz, ezt a következőképpen oldja meg:
                ComboBoxTexturacsomagok.Items.Add("hiányzó fájlok!");   //a gyermekeihez hozzáadja a kiírni kívánt szövege
                ComboBoxTexturacsomagok.SelectedIndex = 0;              //beállítja az aktuális kiválasztás indexét nullára
                ComboBoxTexturacsomagok.IsEnabled = false;              //és deaktiválja a comboboxot.
            }
            else                                           //ha nem tartalék textúracsomagot használunk
            {
                //elhelyezi a comboboxban az itemeket
                for (int i = 0; i < textúracsomagok.Count; i++) //ehhez végigmegy a textúracsomagok listán
                {
                    ComboBoxItem cbi = new ComboBoxItem(); //létrehoz egy ideiglenes ComboBoxItem változót
                    cbi.Background = Brushes.Black;        //ennek hátterét feketére állítja
                    cbi.Content = textúracsomagok[i];      //ennek szövegét a textúracsomagok megfelelő indexéről betölti
                    ComboBoxTexturacsomagok.Items.Add(cbi); //és hozzáadja a comboboxhoz a ComboBoxItem-et
                }

                //mindig azt a textúra csomagot választja ki, amelyik éppen ki van választva a settings-ben
                for (int i = 0; i < ComboBoxTexturacsomagok.Items.Count; i++) //ehhez végigmegy a comboboxon
                    if (((ComboBoxTexturacsomagok.Items[i] as ComboBoxItem).Content as string) == Settings.KiválasztottTextúraCsomag)
                        //ha az adott elem szövege a kiválasztott textúracsomag szövege, akkor
                        ComboBoxTexturacsomagok.SelectedIndex = i; //azt választja ki SelectedIndex-nek

            }
            ComboBoxTexturacsomagok.SelectionChanged += ComboBoxTexturacsomagok_SelectionChanged; //ComboBoxTexturacsomagok_SelectionChanged() metódust feliratkoztatja a ComboBoxTexturacsomagok.SelectionChanged eseményre
            spTextúra.Children.Add(ComboBoxTexturacsomagok); //hozzáadja a comboboxot a belső stack panelhez

            sp.Children.Add(spTextúra); //a belső stackpanelt hozzáadja a külső stackpanelhez

            #endregion

            ujGrid.Children.Add(sp);                        //az új grid gyermekeihez hozzáadja a stackpanelt
            foablak.Content = ujGrid;                       //a főablak Contentjét beállítja az új gridre
        }
        /// <summary>
        /// Kilépéskor bezárja a futó alkalmazást.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonKilepes_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        /// <summary>
        /// Nyit egy MessageBox-ot a játékszabályokkal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonJatekszabalyok_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("- Akkor nyersz, ha minden tüzet beszorítasz. Egy tűz akkor\nvan beszorítva, ha mind a négy oldalán csak tűz vagy kő van.\n- Egyszerre csak egy követ tudsz eltolni.\n- Ha tűzbe kerülsz, meghalsz.\n\nJáték közben használható billentyűparancsok:\n- P: Pillanat-állj (pause)\n- M: háttérzene ki/be\n- Esc: kilépés a pályáról (halál nélkül).\n\nJó játékot kíván a készítő: Ozsvárt Károly!", "Játékszabályok");
        }
        /// <summary>
        /// Betölti a grid mentéséből a főablak gridjébe a tartalmat, majd
        /// a főablak Contentjének beállítja a főablak gridjét
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonVissza_Click(object sender, RoutedEventArgs e)
        {
            foablak.gridFoablak = gridsave;
            foablak.Content = foablak.gridFoablak;
        }

        #endregion

        #region Egyéb események

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            HáttérZene();                        //Megnyitja és elindítja a háttérzenét

            if (Settings.HanyadikPálya == 1)            //Ha az első pályán van a játékos, akkor
                ButtonUjJatek.Content = "Új játék";     //Az új játék gomb szövege Új játék lesz.
            else                                        //különben pedig folytatás (x. pálya)
                ButtonUjJatek.Content = String.Format("folytatás ({0}.pálya)", Settings.HanyadikPálya);
        }
        private void foablak_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape) //ha Escape gombot nyom a felhasználó, akkor
                Application.Current.Shutdown(); //leállítja a futó programot
        }
        private void foablak_Closed(object sender, EventArgs e)
        { //az ablak bezárása után
            Settings.Beállítás_Kimentés(); //elmenti a beállításokat a data fájlba
        }
        void hatterzene_Checked(object sender, RoutedEventArgs e)
        { //ha a háttérzene checkbox be lett pipálva, akkor
            HáttérZeneIndít(); //elindítja a háttérzenét
        }
        void hatterzene_Unchecked(object sender, RoutedEventArgs e)
        { //ha a háttérzene checkboxból ki lett véve a pipa, akkor
            HáttérZeneSzünetel(); //szünetelteti a háttérzenét
        }
        void ComboBoxTexturacsomagok_SelectionChanged(object sender, SelectionChangedEventArgs e)
        { //ha a textúra csomag választó combobox választása meg lett változtatva, akkor
            Settings.KiválasztottTextúraCsomag = ((sender as ComboBox).SelectedItem as ComboBoxItem).Content.ToString(); //a megfelelő beállítást átváltoztatja a comboboxban szereplő értékre
        }
        
        #endregion

        #region FormázóMetódusok

        /// <summary>
        /// Megformázza a bemeneti Buttont a játék stílusának megfelelően.
        /// </summary>
        /// <param name="b"></param>
        private void StílustFormázButton(Button b)
        {
            b.FontSize = 30;
            b.FontFamily = new FontFamily("Chiller");
            b.FontWeight = FontWeights.Bold;
            b.Foreground = Brushes.Black;
            b.BorderBrush = Brushes.White;
            b.Background = Brushes.Red;
        }
        /// <summary>
        /// Megformázza a bemeneti CheckBoxot a játék stílusának megfelelően.
        /// </summary>
        /// <param name="c"></param>
        /// <param name="szöveg"></param>
        /// <param name="betűméret"></param>
        /// <param name="betűszín"></param>
        /// <param name="margó"></param>
        private void StílustFormázCheckBox(CheckBox c, string szöveg, int betűméret, Brush betűszín, Thickness margó)
        {
            c.Content = szöveg;
            c.Foreground = betűszín;
            c.FontSize = betűméret;
            c.Margin = margó;
            c.FontFamily = new FontFamily("Chiller");
        }
        /// <summary>
        /// Megformázza a bemeneti ComboBoxot a játék stílusának megfelelően.
        /// </summary>
        /// <param name="c"></param>
        /// <param name="betűméret"></param>
        /// <param name="szélesség"></param>
        /// <param name="margó"></param>
        private void StílustFormázComboBox(ComboBox c, int betűméret, int szélesség, Thickness margó)
        {
            c.FontSize = betűméret;
            c.FontFamily = new FontFamily("Chiller");
            c.Foreground = Brushes.Red;
            c.BorderBrush = Brushes.Black;
            c.Background = Brushes.Black;
            c.HorizontalAlignment = HorizontalAlignment.Left;
            c.Width = szélesség;
            c.Margin = margó;
            
        }
        /// <summary>
        /// Megformázza a bemeneti Labelt a játék stílusának megfelelően.
        /// </summary>
        /// <param name="l"></param>
        /// <param name="szöveg"></param>
        /// <param name="betűméret"></param>
        /// <param name="szélesség"></param>
        /// <param name="margó"></param>
        private void StílustFormázLabel(Label l, string szöveg, int betűméret, int szélesség, Thickness margó)
        {
            l.Content = szöveg;
            l.FontSize = betűméret;
            l.FontFamily = new FontFamily("Chiller");
            l.Foreground = Brushes.Red;
            l.BorderBrush = Brushes.Black;
            l.Background = Brushes.Black;
            l.HorizontalAlignment = HorizontalAlignment.Left;
            l.Width = szélesség;
            l.Margin = margó;
        }

        #endregion
    }
}