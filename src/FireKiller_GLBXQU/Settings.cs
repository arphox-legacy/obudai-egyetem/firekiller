﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Windows;

namespace FireKiller_GLBXQU
{
    static class Settings
    {
        //Közös beállítások:
        public static bool Háttérzene = true;
        public static string KiválasztottTextúraCsomag = "default";
        public static bool BackupTextúraCsomagHasználata = false;

        //Játékablakhoz tartozó beállítások:
            //ezeket az értékeket SZABAD változtatni:
            public static int TüzekSebessége_ms = 500;
            public static int KövekSzáma = 15;              //ajánlott: 15 (legalább 6 legyen!)

            //ezeket az értékeket NEM AJÁNLOTT változtatni!
            public static int HanyadikPálya = 1;
            public static int TüzekSzáma = 1;               //TILOS változtatni!
            public static int SorokSzáma = 10;
            public static int OszlopokSzáma = 10;
            public static int PályánkéntHányKővelNő = 5;
            public static int Skála = 44;

            //Egyéb:
            public static bool Paused = false;

        //A játék végéhez tartozó beállítások
        public static int Original_KövekSzáma = KövekSzáma;
        public static int Original_TüzekSzáma = TüzekSzáma;
        public static int Original_SorokSzáma = SorokSzáma;
        public static int Original_OszlopokSzáma = OszlopokSzáma;
        public static int Original_Skála = Skála;
        public static int Original_TüzekSebessége_ms = TüzekSebessége_ms;

        //Ellenőrző metódusok
        /// <summary>
        /// Minden fájllal kapcsolatos hibalehetőséget leellenőriz: Ha nincs default textúra, akkor
        /// a Backup csomagot használja. Ha nincsenek meg a háttérzene fájlok, akkor nincs zene.
        /// </summary>
        public static void Hibaellenőrzés()
        {
            if (!Ellenőrző_default_textures())
                BackupTextúraCsomagHasználata = true;
            if (!Ellenőrző_music())
                Háttérzene = false;
        }
        /// <summary>
        /// Megadja, hogy létezik -e music mappa, és ha igen, akkor megtalálhatóak -e benne a
        /// következő fájlok: intro.mp3, music.mp3
        /// </summary>
        /// <returns></returns>
        public static bool Ellenőrző_music()
        {
            if (!Directory.Exists("music"))
                return false;
            DirectoryInfo dir = new DirectoryInfo("music");
            bool intro = false, map = false;
            foreach (FileInfo f in dir.GetFiles())
            {
                if (f.Name == "intro.mp3")
                    intro = true;
                else if (f.Name == "map.mp3")
                    map = true;
            }
            return intro && map;
        }
        /// <summary>
        /// Ellenőrzi, hogy a default textúra létezik -e.
        /// </summary>
        /// <returns></returns>
        public static bool Ellenőrző_default_textures()
        {
            if (!Directory.Exists("textures/default"))
                return false;
            else
                return Ellenőrző_textúracsomag("default"); //default mappa ellenőrzése
        }
        /// <summary>
        /// Ellenőrzi, hogy a stringként megadott könyvtárban megtalálható -e minden
        /// egy textúracsomaghoz szükséges fájl. 
        /// NEM ellenőrzi, hogy a könyvtár létezik -e!
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static bool Ellenőrző_textúracsomag(string könyvtárNév)
        {
            DirectoryInfo dir = new DirectoryInfo("textures/" + könyvtárNév);
            bool path = false, stone = false, player = false, fire = false;
            foreach (FileInfo f in dir.GetFiles())
            {
                if (f.Name == "path.png")
                    path = true;
                else if (f.Name == "stone.png")
                    stone = true;
                else if (f.Name == "player.png")
                    player = true;
                else if (f.Name == "fire.png")
                    fire = true;
            }
            return path && stone && player && fire;
        }

        /// <summary>
        /// Ha textures mappa létezik, akkor betölti az azokban található megfelelő formájú textúra csomagokat.
        /// </summary>
        /// <returns></returns>
        public static List<string> TextúraCsomagokBetöltése()
        {
            if (!Directory.Exists("textures"))
                return null;

            List<string> l = new List<string>();
            DirectoryInfo dir = new DirectoryInfo("textures");
            foreach (DirectoryInfo d in dir.GetDirectories())
                if (Ellenőrző_textúracsomag(d.Name)) //ha az adott könyvtár megfelel textúracsomagként
                    l.Add(d.Name); //akkor hozzáadjuk a listához

            return l;
        }

        //Mentés-betöltő metódus
        /// <summary>
        /// Beállítja a Settings változóit a külső data fájl alapján
        /// </summary>
        public static void Beállítás_Betöltés()
        {
            if (File.Exists("data"))
            {
                try
                {
                    //25. pálya felett maximalizált a SorokSzáma 39-re
                    //30. pálya felett a skála fixen 15
                    //50. pálya felett maximalizált a tüzek és kövek száma
                    //50. pálya felett kétszer akkora ütemben gyorsulnak a tüzek

                    string[] sorok = File.ReadAllLines("data");
                    Háttérzene = Convert.ToBoolean(sorok[0]);
                    HanyadikPálya = int.Parse(sorok[1]);
                    if (HanyadikPálya <= 50) 
                    {
                        TüzekSzáma = HanyadikPálya;
                        KövekSzáma = (KövekSzáma - 5) + HanyadikPálya * 5;
                    }
                    else
                    {
                        TüzekSzáma = 50;
                        KövekSzáma = (KövekSzáma - 5) + PályánkéntHányKővelNő * 50;
                    }
                    if (HanyadikPálya > 25)
                        SorokSzáma = 39;
                    else
                        SorokSzáma = (SorokSzáma - 1) + HanyadikPálya;
                    OszlopokSzáma = SorokSzáma;
                    if (HanyadikPálya >= 30)
                        Skála = 15;
                    else
                        Skála = (Skála + 1) - HanyadikPálya;
                    TüzekSebessége_ms = (TüzekSebessége_ms + 1) - HanyadikPálya;
                    if (HanyadikPálya > 50) //ha 50. pályán túl van, akkor pályánként már 2 ms-sel gyorsulnak a tüzek
                        TüzekSebessége_ms = TüzekSebessége_ms - (HanyadikPálya - 50);
                }
                catch (Exception)
                {
                    MessageBox.Show("Hibás beállításfájlok!\nKérlek töröld a \"data\" fájlt!", "Hiba", MessageBoxButton.OK, MessageBoxImage.Error);
                    Application.Current.Shutdown();
                }
            }
        }
        public static void Beállítás_Kimentés()
        {
            StreamWriter sw = new StreamWriter("data");
            sw.WriteLine(Settings.Háttérzene);
            sw.WriteLine(Settings.HanyadikPálya);
            sw.Close();
        }
    }
}