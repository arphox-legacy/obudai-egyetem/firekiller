﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FireKiller_GLBXQU
{
    abstract class TablaObjektum
    {
        /// <summary>
        /// Az objektum X koordinátája
        /// </summary>
        public int x;
        /// <summary>
        /// Az objektum X koordinátája
        /// </summary>
        public int X
        {
            get { return x; }
            set { x = value; }
        }

        /// <summary>
        /// Az objektum Y koordinátája
        /// </summary>
        public int y;
        /// <summary>
        /// Az objektum Y koordinátája
        /// </summary>
        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        /// <summary>
        /// Egy egységgel mozog az adott irányba
        /// </summary>
        /// <param name="irány"></param>
        public void Mozog(Irány irány)
        {
            if (irány == Irány.Balra)
                y--;
            else if (irány == Irány.Jobbra)
                y++;
            else if (irány == Irány.Fel)
                x++;
            else
                x--;
        }
    }
}