﻿using System;
using System.Windows;
using System.Collections.Generic;

namespace FireKiller_GLBXQU
{
    //A Palya osztályban csak legenerálódik a pálya, a felhelyezést JatekWindow osztály csinálja.
    public class Palya
    {
        byte[,] tábla;                          //ebben van a tábla aktuális állapota
        public byte[,] Tábla
        {
            get { return tábla; }
        }                   
        List<Tuz> tüzek = new List<Tuz>();      //ebben a listában vannak a tüzek
        internal List<Tuz> Tüzek
        {
            get { return tüzek; }
        }
        List<Point> pontok = new List<Point>(); //ebben találhatók az objektumok koordinátái
        Random r = new Random();

        Point játékosKezdetiPozíció = new Point();
        public Point JátékosKezdetiPozíció
        {
            get { return játékosKezdetiPozíció; }
        }
        //beállítások betöltése a beállítás fájlból
        int kövekszáma = Settings.KövekSzáma;
        int tüzekszáma = Settings.TüzekSzáma;
        int sorok = Settings.SorokSzáma;
        int oszlopok = Settings.OszlopokSzáma;

        public Palya()
        { //a konstruktor lefutásakor
            RandomPályaGenerál();               //generál egy random pályát
            JátékosTűzMellettVanKorrigál();     //és ha esetleg játékos mellé tűz került volna, akkor ezt korrigálja
        }

        /// <summary>
        /// Megadja, hogy az összes tűz be van-e kerítve
        /// </summary>
        public bool ÖsszesTűzBekerítve
        {
            get
            {
                for (int i = 0; i < tüzek.Count; i++)
                    if (tüzek[i].Bekerítve == false)
                        return false;
                return true;
            }
        }

        /// <summary>
        /// Generál egy random pályát a megadott sor/oszlop/kődarab érték alapján
        /// </summary>
        private void RandomPályaGenerál()
        {
            tábla = new byte[sorok, oszlopok];
            //kiszámoljuk a kövekszáma db. kő, tüzekszáma db. tűz és egy játékos koordinátáit
            for (int i = 0; i < kövekszáma + tüzekszáma + 1; i++)
            //+1 azért kell, mert az utolsó indexen lesz a játékos pontja (abból csak egy kell)
            {
                Point p = new Point(r.Next(sorok), r.Next(oszlopok));   //felvesz egy random pontot
                while (pontok.Contains(p)) //amíg van ilyen pont, akkor
                    p = new Point(r.Next(sorok), r.Next(oszlopok)); //újat generálunk
                pontok.Add(p); //mivel itt már megfelelő a pont, hozzá adja a pontok listához
            }
            //ezek után berakjuk a táblába a pontokat
            for (int i = 0; i < pontok.Count; i++)
            {
                if (i >= kövekszáma && i < pontok.Count - 1) //itt vannak a tüzek koordinátái
                {
                    tábla[(int)pontok[i].X, (int)pontok[i].Y] = 3;
                    tüzek.Add(new Tuz((int)pontok[i].   X, (int)pontok[i].Y));
                }
                else if (i == pontok.Count - 1) //utolsó indexen van a játékos pontja
                {
                    tábla[(int)pontok[i].X, (int)pontok[i].Y] = 2;
                    játékosKezdetiPozíció.X = pontok[i].X;
                    játékosKezdetiPozíció.Y = pontok[i].Y;
                }
                else //minden más kő
                    tábla[(int)pontok[i].X, (int)pontok[i].Y] = 1;
            }
        }

        /// <summary>
        /// A pályagenerálás után, amíg a játékos tűz mellett van, akkor átrakja a játékost.
        /// </summary>
        private void JátékosTűzMellettVanKorrigál()
        {
            if ((VanMelletteTűz((int)játékosKezdetiPozíció.X, (int)játékosKezdetiPozíció.Y))) //ha van a játékos mellett tűz
                tábla[(int)játékosKezdetiPozíció.X, (int)JátékosKezdetiPozíció.Y] = 0; //akkor a játékos helyét nullára állítja
            //és keres neki egy másik helyet
            while (VanMelletteTűz((int)játékosKezdetiPozíció.X, (int)játékosKezdetiPozíció.Y)) //amíg van a játékos mellett tűz, addig
            {
                Point p = new Point(r.Next(sorok), r.Next(oszlopok)); //generál egy új véletlen pontot
                while (pontok.Contains(p))                         //amíg esetleg már lenne ilyen pont, akkor
                    p = new Point(r.Next(sorok), r.Next(oszlopok));  //újat generálunk
                játékosKezdetiPozíció.X = p.X;          //és a játékos kezdeti pozíciójának
                játékosKezdetiPozíció.Y = p.Y;          //beállítja a pont X és Y koordinátáját
            }
            tábla[(int)játékosKezdetiPozíció.X, (int)JátékosKezdetiPozíció.Y] = 2; //ezután a tábla megfelelő indexét frissíti, jelezve, hogy ott az ember
        }
        /// <summary>
        /// Megadja, hogy az adott koordináta bejárható-e és ha igen, akkor
        /// mellette (alatt, felett, balra, jobbra) található-e tűz
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private bool VanMelletteTűz(int x, int y)
        {
            if ((x + 1 < tábla.GetLength(0) && tábla[x + 1, y] == 3) ||
                (x - 1 >= 0 && tábla[x - 1, y] == 3) ||
                (y + 1 < tábla.GetLength(1) && tábla[x, y + 1] == 3) ||
                (y - 1 >= 0 && tábla[x, y - 1] == 3))
                return true;
            else
                return false;
        }
    }
}