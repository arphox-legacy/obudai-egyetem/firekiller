﻿namespace FireKiller_GLBXQU
{
    class Tuz : TablaObjektum
    {
        public Tuz(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        /// <summary>
        /// Megadja, hogy a tűz éppen be van-e kerítve
        /// </summary>
        bool bekerítve = false;
        /// <summary>
        /// [Get/Set] Megadja, hogy a tűz éppen be van-e kerítve
        /// </summary>
        public bool Bekerítve
        {
            get { return bekerítve; }
            set { bekerítve = value; }
        }
    }
}